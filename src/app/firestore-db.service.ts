import { Injectable } from "@angular/core";
import {
  AngularFirestore,
  AngularFirestoreCollection,
  AngularFirestoreDocument,
  DocumentReference
} from "@angular/fire/firestore";
import { map, take } from "rxjs/operators";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class FirestoreDbService {
  private agreemants: Observable<Agreemant[]>;
  private agreemantCollection: AngularFirestoreCollection<Agreemant>;
  private posts: Observable<Post[]>;
  private postCollection: AngularFirestoreCollection<Post>;

  private comments: Observable<Comment[]>;
  private commentCollection: AngularFirestoreCollection<Comment>;

  constructor(private afs: AngularFirestore) {
    this.agreemantCollection = this.afs.collection<Agreemant>("Agreemants");
    this.agreemants = this.agreemantCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }

  async initPosts(agreemantID) {
    this.postCollection = this.afs.collection<Post>(
      `Agreemants/${agreemantID}/Posts`
    );
    this.posts = this.postCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          // console.log(data);
          return { id, ...data };
        });
      })
    );
    return this.posts;
  }
  async initComments(agreemantID, postID) {
    console.log("init Comments");
    this.commentCollection = this.afs.collection<Comment>(
      `Agreemants/${agreemantID}/Posts/${postID}/Comments`
    );
    this.comments = this.commentCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
    return this.comments;
  }
  getAgreemants(): Observable<Agreemant[]> {
    return this.agreemants;
  }
  getAgreemant(id: string): Observable<Agreemant> {
    return this.agreemantCollection
      .doc<Agreemant>(id)
      .valueChanges()
      .pipe(
        take(1),
        map(agreemant => {
          agreemant.id = id;
          return agreemant;
        })
      );
  }
  addAgreemant(agreemant: Agreemant): Promise<DocumentReference> {
    return this.agreemantCollection
      .doc(agreemant.id)
      .set(agreemant)
      .then();
  }
  updateAgreemant(agreemant: Agreemant): Promise<void> {
    return this.agreemantCollection
      .doc(agreemant.id)
      .update({ name: agreemant.customerName });
  }
  deleteAgreemant(id: string): Promise<void> {
    return this.agreemantCollection.doc(id).delete();
  }

  getPosts(): Observable<Post[]> {
    return this.posts;
  }
  getPost(id: string): Observable<Post> {
    return this.postCollection
      .doc<Post>(id)
      .valueChanges()
      .pipe(
        take(1),
        map(post => {
          post.id = id;
          return post;
        })
      );
  }
  addPost(post: Post): Promise<DocumentReference> {
    return this.postCollection.add(post);
  }
  updatePost(post: Post): Promise<void> {
    return this.postCollection
      .doc(post.id)
      .update({ photos: post.photos, notes: post.dateAdded });
  }
  deletePost(id: string): Promise<void> {
    return this.postCollection.doc(id).delete();
  }

  getComments(): Observable<Comment[]> {
    return this.comments;
  }
  getComment(id: string): Observable<Comment> {
    return this.commentCollection
      .doc<Comment>(id)
      .valueChanges()
      .pipe(
        take(1),
        map(comment => {
          comment.id = id;
          return comment;
        })
      );
  }
  addComment(comment: Comment): Promise<DocumentReference> {
    return this.commentCollection.add(comment);
  }
  updateComment(comment: Comment): Promise<void> {
    return this.commentCollection
      .doc(comment.id)
      .update({ comment: comment.comment, user: comment.user });
  }
  deleteComment(id: string): Promise<void> {
    return this.commentCollection.doc(id).delete();
  }
}

//interface for firebase agreemant
export interface Agreemant {
  id?: string;
  agNo: string;
  customerName: string;
}
//inteface for firebase posts data access
export interface Post {
  id?: string;
  dateAdded: string;
  photos: Array<string>;
}

export interface Comment {
  id?: string;
  comment: string;
  user: string;
  dateAdded: string;
}
