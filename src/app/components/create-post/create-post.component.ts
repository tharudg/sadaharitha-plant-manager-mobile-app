import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-create-post",
  templateUrl: "./create-post.component.html",
  styleUrls: ["./create-post.component.scss"]
})
export class CreatePostComponent implements OnInit {
  constructor(private actRoute: ActivatedRoute) {}

  ngOnInit() {
    this.actRoute.queryParams.subscribe(res => console.log(res));
  }
}
