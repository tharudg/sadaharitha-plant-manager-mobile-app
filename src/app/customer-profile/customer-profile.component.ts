import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { NavController } from "@ionic/angular";
import { AngularFirestore } from "@angular/fire/firestore";
import { FirestoreDbService } from "../firestore-db.service";

@Component({
  selector: "app-customer-profile",
  templateUrl: "./customer-profile.component.html",
  styleUrls: ["./customer-profile.component.scss"]
})
export class CustomerProfileComponent implements OnInit {
  agreemant;
  posts = [];
  constructor(
    private router: Router,
    private actRoute: ActivatedRoute,
    private fdbs: FirestoreDbService
  ) {}

  ngOnInit() {
    this.actRoute.queryParams.subscribe(data => {
      this.agreemant = data;
      this.fdbs.initPosts(data.id).then(res => {
        this.fdbs.getPosts().subscribe(res => {
          this.posts = res;
        });
      });
    });
  }
  view(post, index) {
    post["agno"] = this.agreemant.id;
    this.router.navigate([`details/${this.agreemant.id}/${post.id}/chat`], {
      queryParams: post
    });
  }
  addPost() {
    this.router.navigate([`details/${this.agreemant.id}/new-post`], {
      queryParams: this.agreemant
    });
  }
}
