import { NgModule } from "@angular/core";
import { PreloadAllModules, RouterModule, Routes } from "@angular/router";
import { CustomerProfileComponent } from "./customer-profile/customer-profile.component";
import { ChatComponent } from "./chat/chat.component";
import { CreatePostComponent } from "./components/create-post/create-post.component";

const routes: Routes = [
  {
    path: "",
    loadChildren: () => import("./tabs/tabs.module").then(m => m.TabsPageModule)
  },
  {
    pathMatch: "full",
    path: "details/:id",
    component: CustomerProfileComponent
  },
  {
    path: "details/:id/:id/chat",
    component: ChatComponent,
    pathMatch: "full"
  },
  {
    path: "details/:id/new-post",
    component: CreatePostComponent,
    pathMatch: "full"
  }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
