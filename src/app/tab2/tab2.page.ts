import { Component, OnInit } from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";
import { Router } from "@angular/router";
import { map, take } from "rxjs/operators";
import { FirestoreDbService, Agreemant } from "../firestore-db.service";

@Component({
  selector: "app-tab2",
  templateUrl: "tab2.page.html",
  styleUrls: ["tab2.page.scss"]
})
export class Tab2Page implements OnInit {
  objectList: any[];
  loadedList: any[];
  hasInput: boolean = false;

  constructor(private router: Router, private fdbs: FirestoreDbService) {}

  onClick(agreemant, i) {
    console.log(agreemant);
    this.router.navigate([`details/${agreemant.id}`], {
      queryParams: agreemant
    });
  }
  ngOnInit() {
    this.fdbs.getAgreemants().subscribe(res => {
      this.objectList = res;
      this.loadedList = res;
      console.log(this.objectList);
    });

    // this.fdbs.initPosts("20201604").then(res => {
    //   this.fdbs.getPosts().subscribe(res => console.log(res));
    // });
  }

  initializeItems(): void {
    this.objectList = this.loadedList;
  }

  filterList(evt) {
    this.initializeItems();
    const searchTerm = evt.srcElement.value;
    if (!searchTerm || searchTerm == "" || searchTerm == " ") {
      this.hasInput = false;
      return;
    }
    if (searchTerm) {
      this.hasInput = true;
      this.objectList = this.objectList.filter(agreemant => {
        if (agreemant.id && searchTerm) {
          if (
            agreemant.id.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1
          ) {
            console.log(agreemant.id);
            return true;
          }
          return false;
        }
      });
      console.log(searchTerm);
    }
  }
}
