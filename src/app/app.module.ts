import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { RouteReuseStrategy } from "@angular/router";

import { IonicModule, IonicRouteStrategy } from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { environment } from "src/environments/environment";
import { AngularFireModule } from "@angular/fire";
import { AngularFireDatabaseModule } from "@angular/fire/database";

import { AngularFirestoreModule } from "@angular/fire/firestore";
import { CommonModule } from "@angular/common";
import { CustomerProfileComponent } from "./customer-profile/customer-profile.component";
import { ChatComponent } from "./chat/chat.component";
import { FirestoreDbService } from "./firestore-db.service";
import { FormsModule } from "@angular/forms";
import { CreatePostComponent } from "./components/create-post/create-post.component";

@NgModule({
  declarations: [
    AppComponent,
    CustomerProfileComponent,
    ChatComponent,
    CreatePostComponent
  ],
  entryComponents: [],
  imports: [
    CommonModule,
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule,
    AngularFireDatabaseModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    FirestoreDbService,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
