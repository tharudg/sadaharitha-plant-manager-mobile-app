import { Component, OnInit, ViewChild } from "@angular/core";
import { IonInfiniteScroll, IonContent } from "@ionic/angular";
import { FirestoreDbService } from "../firestore-db.service";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-chat",
  templateUrl: "./chat.component.html",
  styleUrls: ["./chat.component.scss"]
})
export class ChatComponent implements OnInit {
  @ViewChild("content", { static: false }) content: IonContent;
  infiniteScroll: IonInfiniteScroll;
  post;
  comments;
  newComment;
  constructor(
    private fdbs: FirestoreDbService,
    private actRouter: ActivatedRoute
  ) {}

  ngOnInit() {
    this.actRouter.queryParams.subscribe(res => {
      this.post = res;

      this.fdbs.initComments(this.post.agno, this.post.id).then(res => {
        this.fdbs.getComments().subscribe(res => {
          // if (res) {
          //   console.log(res);
          //   return (this.comments = null);
          // }
          this.comments = res.sort((a, b) =>
            a.dateAdded.localeCompare(b.dateAdded)
          );
          this.scrollToBottomOnInit();
        });
      });
    });
  }

  toggleInfiniteScroll() {
    this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
  }

  sendComment() {
    if (this.newComment) {
      console.log("comment available");
      this.fdbs
        .addComment({
          comment: this.newComment,
          dateAdded: Date.now().toString(),
          user: "me"
        })
        .then(res => (this.newComment = null));
    } else {
      console.log("no comment");
    }
  }
  scrollToBottomOnInit() {
    setTimeout(() => {
      if (this.content.scrollToBottom) {
        this.content.scrollToBottom(400);
      }
    }, 500);
  }
}
