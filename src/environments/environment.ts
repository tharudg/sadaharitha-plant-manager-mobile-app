// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  firebaseConfig: {
    apiKey: "AIzaSyBNZOfNz591PXXtjCiBQtZTFx-fIX-NGIY",
    authDomain: "sadaharitha-plant-manager.firebaseapp.com",
    databaseURL: "https://sadaharitha-plant-manager.firebaseio.com",
    projectId: "sadaharitha-plant-manager",
    storageBucket: "sadaharitha-plant-manager.appspot.com",
    messagingSenderId: "891117669328",
    appId: "1:891117669328:web:d966b1f95965643966c7d5",
    measurementId: "G-R3XL76NQQP"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
